#include "midcousre_project.h"

midcousre_project::midcousre_project(QWidget* parent)
    : QWidget(parent)
{
    ui.setupUi(this);
    lineEdit_parent_x = ui.lineEdit_parent_x;
    lineEdit_parent_y = ui.lineEdit_parent_y;
    lineEdit_child_z = ui.lineEdit_child_z;
    button_parrent_info = ui.button_parrent_info;
    button_child_info = ui.button_child_info;
    button_parent_calculate = ui.button_parent_calculate;
    button_child_calculate = ui.button_child_calculate;
    label_info_output = ui.label_info_output;
    label_child_result = ui.label_child_result;
    label_parent_result = ui.label_parent_result;
}

midcousre_project::~midcousre_project() {}

MyClass::MyClass(int field1, int field2)
{
    this->field1 = field1;
    this->field2 = field2;
}

QString MyClass::info_output() const
{
    return QString("x = %1 ; y =: %2").arg(field1).arg(field2);
}

QString MyClass::process_data()
{
    if (0.0 == field2)
        return QString("Devision by zero");

    int result = field1 / field2;
    qDebug() << "MyClass::process_data: " << result;
    return QString("x / y = %1").arg(result);
}

MySubClass::MySubClass(int field1, int field2, double field3) : MyClass(field1, field2)
{
    this->field3 = field3;
}

QString MySubClass::info_output() const
{
    return QString("x = %1 ; y = %2 ; z = %3").arg(field1).arg(field2).arg(field3);
}

QString MySubClass::process_data()
{
    if(0.0 == field3)
        return QString("Devision by zero");

    double result = (static_cast<double>(field1) / field3) + (static_cast<double>(field2) / field3);
    qDebug() << "MySubClass::process_data: " << result;
    return QString("Result: %1").arg(result);
}

void midcousre_project::on_button_parrent_info_clicked()
{
    int field1 = lineEdit_parent_x->text().toInt();
    int field2 = lineEdit_parent_y->text().toInt();
    MyClass obj = MyClass(field1, field2);

    label_info_output->setText(obj.info_output());
}

void midcousre_project::on_button_child_info_clicked()
{
    int field1 = lineEdit_parent_x->text().toInt();
    int field2 = lineEdit_parent_y->text().toInt();
    double field3 = lineEdit_child_z->text().toInt();
    MySubClass obj = MySubClass(field1, field2, field3);

    label_info_output->setText(obj.info_output());
}

void midcousre_project::on_button_parent_calculate_clicked()
{
    int field1 = lineEdit_parent_x->text().toInt();
    int field2 = lineEdit_parent_y->text().toInt();
    MyClass obj = MyClass(field1, field2);

    label_parent_result->setText(obj.process_data());
}

void midcousre_project::on_button_child_calculate_clicked()
{
    int field1 = lineEdit_parent_x->text().toInt();
    int field2 = lineEdit_parent_y->text().toInt();
    double field3 = lineEdit_child_z->text().toInt();
    MySubClass obj = MySubClass(field1, field2, field3);

    label_child_result->setText(obj.process_data());
}
