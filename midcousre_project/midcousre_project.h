#pragma once

#include "ui_midcousre_project.h"
#include <QtWidgets/QWidget>

class midcousre_project : public QWidget
{
    Q_OBJECT

public:
    midcousre_project(QWidget* parent = nullptr);
    ~midcousre_project();

private slots:
    void on_button_parrent_info_clicked();
    void on_button_child_info_clicked();
    void on_button_parent_calculate_clicked();
    void on_button_child_calculate_clicked();

private:
    Ui::midcousre_projectClass ui;
    QLineEdit* lineEdit_parent_x;
    QLineEdit* lineEdit_parent_y;
    QLineEdit* lineEdit_child_z;
    QPushButton* button_parrent_info;
    QPushButton* button_child_info;
    QPushButton* button_parent_calculate;
    QPushButton* button_child_calculate;
    QLabel* label_info_output;
    QLabel* label_child_result;
    QLabel* label_parent_result;
};

class MyClass
{
public:
    MyClass(int field1, int field2);
    QString info_output() const;
    QString process_data();
protected:
    int field1;
    int field2;
};

class MySubClass : public MyClass
{
public:
    MySubClass(int field1, int field2, double field3);
    QString info_output() const;
    QString process_data();
private:
    double field3;
};
